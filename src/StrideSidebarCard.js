import '@atlaskit/css-reset'
import './StrideSidebarCard.css'

import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { AvatarGroup } from '@atlaskit/avatar'
import DropdownMenu, { DropdownItem } from '@atlaskit/dropdown-menu'
import Button from '@atlaskit/button'
import MoreIcon from '@atlaskit/icon/glyph/more'

export default class StrideSidebarCard extends Component {
  static propTypes = {
    logo: PropTypes.string,
    title: PropTypes.string,
    icon: PropTypes.string,
    caption: PropTypes.string,
    description: PropTypes.string,
    actions: PropTypes.array,
    people: PropTypes.array
  }

  render() {
    return (
      <div className="atStrideSidebarCard">
        <div className="atStrideSidebarCard__Top">
          <img
            className="atStrideSidebarCard__Logo"
            src={this.props.logo}
            alt="logo"
          />
          <span className="atStrideSidebarCard__Title">
            {this.props.title}
          </span>
        </div>
        <div className="atStrideSidebarCard__Main">
          <div className="atStrideSidebarCard__Data">
            <img
              className="atStrideSidebarCard__Icon"
              src={this.props.icon}
              alt="icon"
            />
            <div className="atStrideSidebarCard__Info">
              <div className="atStrideSidebarCard__Caption">
                {this.props.caption}
              </div>
              <div className="atStrideSidebarCard__Descr">
                {this.props.description}
              </div>
            </div>
            <div className="atStrideSidebarCard__Menu">
              <DropdownMenu
                shouldFlip={false}
                position="bottom right"
                trigger={<Button iconBefore={<MoreIcon label="" />} />}
              >
                {this.props.actions.map((action, i) => (
                  <DropdownItem
                    key={i}
                    onClick={action.callback}
                  >
                    {action.name}
                  </DropdownItem>
                ))}
              </DropdownMenu>
            </div>
          </div>
          <div className="atStrideSidebarCard__People">
            <AvatarGroup
              appearance="stack"
              data={this.props.people}
              size="small"
            />
          </div>
        </div>
      </div>
    )
  }
}
