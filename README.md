## Nice looking card for Atlassian Stride sidebar apps written in React

You can find demo [here](https://bitbucket.org/atlassian/stride-sidebar-card-demo)

![card-sample](https://bytebucket.org/atlassian/stride-sidebar-card/raw/af553ee7e1053600fce018461c02eba58f32c149/card-sample.png)


### How to install
```
npm i --save stride-sidebar-card
```
Be sure to install all peer dependencies:
```
npm i --save @atlaskit/avatar @atlaskit/button @atlaskit/dropdown-menu @atlaskit/icon prop-types react react-dom
```

### Usage example
```jsx
import React from 'react'
import { render } from 'react-dom'

import StrideSidebarCard from 'stride-sidebar-card'

render(
  <div style={{
    width: 240,
    margin: 50
  }}>
    <StrideSidebarCard
      logo="https://wac-cdn.atlassian.com/dam/jcr:89e146b4-642e-41fc-8e65-7848337d7bdd/atlassian_charlie_square.png"
      title="Very nice card with really long description text that can't just fit"
      icon="https://assets.materialup.com/uploads/cf7d01cf-c038-44e6-b5fa-e9a52b5333fb/avatar.jpg"
      caption="Too long caption to fit here as is"
      description="Lorem ipsum dolor sit amet"
      actions={[
        {
          name: 'Say hello',
          callback: () => alert('hello!')
        },
        {
          name: 'Say hi',
          callback: () => alert('hi!')
        }
      ]}
      people={[
        {
          src: 'https://findicons.com/files/icons/1072/face_avatars/300/a02.png',
          name: 'John Doe'
        },
        {
          src: 'http://icons.iconarchive.com/icons/hopstarter/face-avatars/256/Male-Face-C4-icon.png',
          name: 'Michael Jackson'
        }
      ]}
    />
  </div>,
  document.getElementById('root')
)
```
